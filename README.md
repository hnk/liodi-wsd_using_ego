# LiODi - Group IIa - Word Sense Disambiguation using Extended Gloss Overlaps


# Requirements
- linux system (or a functioning bash shell)
- java 8
- python > 3.5 and pip


# Project Set-Up
Wordnet, Corpus data and blazegraph is managed by [git-lfs][1]. If git-lfs is installed on your system, you can run `git lfs fetch master` to grab all neccessary files.

If you cannot install git-lfs locally, you must aquire all neccessary files by yourself and import the data into blazegraph.
You need:
* blazegraph.jar: [Download latest version][2]
* wordnet rdf export: [Download latest version][3]
* corpus data: [Test][4] and [Train][5]
* CoNLLRDF tool: [Download latest version][6]

[1]: https://git-lfs.github.com/
[2]: https://sourceforge.net/projects/bigdata/files/latest/download
[3]: http://wordnet-rdf.princeton.edu/
[4]: https://moodle.studiumdigitale.uni-frankfurt.de/moodle2/pluginfile.php/39017/mod_wiki/attachments/601//en-ud-test.conllu.gz?forcedownload=1
[5]: https://moodle.studiumdigitale.uni-frankfurt.de/moodle2/pluginfile.php/39017/mod_wiki/attachments/601//en-ud-train.conllu.gz?forcedownload=1
[6]: https://moodle.studiumdigitale.uni-frankfurt.de/moodle2/course/view.php?id=348#section-21

# Configuration
Run `python3 -m venv venv` to configure a virtualenv.  
Run `source venv/bin/activate` to use virtualenv.  
Run ` pip install -r requirements.txt` to install requirements into virtualenv.

# Execution
It should suffice to execute `run.sh` in the main forlder.