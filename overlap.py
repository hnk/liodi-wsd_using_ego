"""Implements the complete Extended Gloss Overlap algorithm."""

from nltk.corpus import stopwords

STOPWORDS = set(stopwords.words('english'))

def longest_common_substring(sentence_one, sentence_two):
    """Implements the complete Extended Gloss Overlap algorithm."""
    longest_match = 0
    index1, index2 = 0, 0
    for i in range(0, len(sentence_one)):
        for j in range(0, len(sentence_two)):
            if sentence_one[i] == sentence_two[j] and sentence_one[i] not in STOPWORDS:
                match_length = 1
                for forward in range(1, min(len(sentence_one) - i, len(sentence_two) - j)):
                    if sentence_one[i+forward] != sentence_two[j+forward]:
                        break
                    match_length += 1
                if match_length > longest_match:
                    for reverse in range(i + match_length - 1, i, -1):
                        if sentence_one[reverse] in STOPWORDS:
                            match_length -= 1
                        else:
                            break
                    if match_length > longest_match:
                        longest_match = match_length
                        index1, index2 = i, j
    return longest_match, index1, index2

def overlap_score(sentence_one, sentence_two):
    """Implements the complete Extended Gloss Overlap algorithm."""
    score = 0
    while True:
        length, index1, index2 = longest_common_substring(sentence_one, sentence_two)
        if length == 0:
            break
        score += length ** 2
        del sentence_one[index1:(index1 + length)]
        del sentence_two[index2:(index2 + length)]
    return score
