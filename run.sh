#!/bin/sh

# the directory of the script
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

exitfn () {
    printf '\n'
    printf '%s\n' 'Cleaning up.'

    if [[ -n $BLAZE_PID ]]; then
        printf 'Stopping blazegraph\n'
        kill -s 15 $BLAZE_PID
        while $(kill -0 $BLAZE_PID &>/dev/null); do
            printf '.'
        done
        printf '\n'
    fi

    printf 'Run complete. Exiting.\n'
    exit $1
}

trap "exitfn" SIGINT

if [[ ! "$DIR/venv" || ! -d "$DIR/venv" ]]; then
    printf '\n'
    printf '%s\n' 'Python venv folder not available!'
    printf '%s\n' 'Install with "python3 -m venv venv"'
    printf '%s\n' 'Then load the venv with "source venv/bin/activate"'
    printf '%s\n' 'and run "pip install -r requirements.txt"'
    exitfn 1
fi

if [[ ! "$DIR/graph_db/blazegraph.jar" ]]; then
    printf '%s\n' '"blazegraph.jar" missing in $DIR/graph_db'
    exitfn 1
fi

echo "" > $DIR/graph_db/blazegraph.log
echo "" > $DIR/graph_db/blazegraph.error.log

cd $DIR/graph_db
java -server -Xmx4g -jar blazegraph.jar >blazegraph.log 2>blazegraph.error.log & export BLAZE_PID=$!
cd $DIR

# thanks to http://phptest.club/t/how-to-know-if-selenium-server-is-ready/277/2
printf 'Waiting for Blazegraph to start\n'
until $(curl --output /dev/null --silent --head --fail http://192.168.1.103:9999/blazegraph); do
    printf '.'
done
printf '\n\n'
printf 'Blazegraph ready, beginning run\n'
printf '\n'

echo "" > $DIR/error.log

venv/bin/python3 wsd.py 2>error.log

if [[ $? -ne 0 ]]; then
    printf '%s\n' 'Python exited with an error. Check "error.log" for details.'
fi

exitfn 0
