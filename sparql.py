import warnings

# 3rd party imports
from SPARQLWrapper import JSON

"""Collects all sparql queries needed for the wsd algorithm."""

# prefixes to be passed to blazegraph db
PREFIXES = """\
    prefix :      <https://github.com/UniversalDependencies/UD_English>
    prefix conll: <http://ufal.mff.cuni.cz/conll2009-st/task-description.html#>
    prefix nif:   <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#>
    prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#>
    prefix terms: <http://purl.org/acoli/open-ie/>
    prefix wn:    <http://wordnet-rdf.princeton.edu/wn31/>
    prefix wno:   <http://wordnet-rdf.princeton.edu/ontology#>
    prefix xsd:   <http://www.w3.org/2001/XMLSchema#>

    prefix lod:   <http://acoli.informatik.uni-frankfurt.de/courses/ss2017-bwvbs.html#>
    """

def q_c_all_sentences(limit=0):
    """Returns a sparql query returning all sentences, ordered by uri, within corpus data.

    Keyword arguments:
    limit -- limit the number of sentences returned (default 0)
    """

    s = """\
    {prefix}

    select ?uri where {{
        ?uri a nif:Sentence
    }} ORDER BY ?uri{limit}
    """
    return s.format(prefix=PREFIXES, limit=((" LIMIT " + str(limit)) if limit > 0 else ""))

def q_c_words_in_sentence(uri, limit=0):
    """Returns a sparql query returning all words, ordered by id, within a sentence.

    Keyword arguments:
    uri -- uri to sentence in blazegraph
    limit -- limit the number of sentences returned (default 0)
    """

    s = """\
    {prefix}

    SELECT ?r ?id ?word ?lemma ?upos WHERE {{
        ?name conll:HEAD* <{uri}> .
        ?name conll:ID* '1' .
        ?name nif:nextWord* ?r .
        ?r conll:ID ?id .
        ?r conll:WORD ?word .
        ?r conll:LEMMA ?lemma .
        ?r conll:UPOS ?upos 
    }} ORDER BY xsd:integer(?id){limit}
    """
    return  s.format(prefix=PREFIXES, uri=uri, limit=((" LIMIT " + str(limit)) if limit > 0 else ""))

def i_c_synset_for_word(word, synset, prefix=PREFIXES):
    """Inserts a new link between a word in the corpus and a determined synset within wordnet.

    Keyword arguments:
    word -- the uri of the word in blazegraph
    synset -- the uri of the synset in blazegraph
    """

    s = """\
    {prefix}

    INSERT {{
        <{word}> lod:synset <{synset}>
    }}
    WHERE {{
        <{word}> a nif:Word
    }}
    """
    return s.format(prefix=PREFIXES, word=word, synset=synset)

def q_w_get_synsets_for_word(word, pos, lang="eng", limit=0):
    """Returns a sparql query returning all wordnet synsets, ordered by uri, for a given word and part-of-speech tag.

    Keyword arguments:
    word -- word to be searched for
    pos -- part-of-speech tag for word
    lang -- language of word (default 'eng')
    limit -- limit the number of sentences returned (default 0)
    """


    # this is slower...
    # s = """\
    # {prefix}

    # SELECT ?synset WHERE {{
    #     ?synset wno:part_of_speech wno:{pos} .
    #     ?synset rdfs:label "{word}"@{lang}
    # }}{limit}
    # """

    if pos == "ADV":
        pos = "adverb"

    if pos == "ADJ":
        pos = "adjective"

    s = """\
    {prefix}

    SELECT ?synset WHERE {{
        ?synset wno:part_of_speech wno:{pos} .
        ?synset rdfs:label "{word}"@{lang} .
        ?synset a wno:Synset
    }}{limit}
    """
    return s.format(prefix=PREFIXES, pos=pos.lower(), word=word, lang=lang, limit=((" LIMIT " + str(limit)) if limit > 0 else ""))

def q_w_get_gloss_for_synset(synset):
    """Returns a sparql query returning the gloss for a given synset.

    Keyword arguments:
    synset -- synset to search for
    """

    s = """\
    {prefix}

    SELECT ?gloss WHERE {{
        <{synset}> wno:gloss ?gloss
    }}
    """
    return s.format(prefix=PREFIXES, synset=synset)

def q_w_get_related_synsets(synset, relation, limit=0):
    """Returns a sqarql query returning all related synsets for a given synset based on relation type.

    Keyword arguments:
    synset -- synset to search for
    relation -- synset relation, i.e. hyponym, part_meronym
    limit -- limit the number of sentences returned (default 0)
    """

    s = """\
    {prefix}

    SELECT ?related WHERE {{
        <{synset}> wno:{relation} ?related
    }}{limit}
    """
    return s.format(prefix=PREFIXES, synset=synset, relation=relation, limit=((" LIMIT " + str(limit)) if limit > 0 else ""))

def run_query(target, query, query_type="GET", set_json=True):
    """Runs a given sparql query agains a target sparql endpoint

    Keyword arguments:
    target -- must be an initialized SPARQLWrapper
    query -- the complete SPARQL query, including prefixes, as a string
    """
    target.setQuery(query)
    target.setMethod(query_type)
    if set_json:
        target.setReturnFormat(JSON)
        return target.queryAndConvert()
    else:
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            return target.query()
