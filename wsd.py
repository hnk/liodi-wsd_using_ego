# 3rd party imports
from SPARQLWrapper import SPARQLWrapper, JSON

# local imports
import overlap
import sparql

"""This module does the actual wsd by going through all sentences within a corpus imported into blazegraph."""

# Configure amount of sentences to work with. 0 means all within the database. Set to small value (i.e. 5) for testing purposes.
SENTENCE_LIMIT = 1

# word types we want to disambiguate and to use for disambiguation
WORD_TYPES = ["NOUN", "VERB", "ADJ", "ADV"]

RELATIONS = {
    "NOUN": ["gloss", "hypernym", "hyponym", "part_meronym", "part_holonym"],
    "VERB": ["gloss", "hypernym", "hyponym"],
    "ADJ":  ["gloss", "similar", "also"],
    "ADV":  ["gloss",],
}

def write_results(best_glosses):
    for (synset, gloss) in best_glosses:
        query = sparql.i_c_synset_for_word(words[int(id)-1]["r"]["value"], synset)
        result = sparql.run_query(corpus, query, query_type="POST", set_json=False)

def print_results(single=False):
    print("    Word to disambiguate: {}".format(target_words[i][1]))
    if not single:
        for w in woc:
            print("    WOC: {}".format(target_words[w][1]))
        print("    SenseScore: {}".format(sensescore))
        print("    Best guesses:")
    else:
        print("    Word has only one possible meaning:")
    for (synset, gloss) in best_glosses:
        print("    - {}".format(gloss))
    print("    Time to disambiguate: {} seconds".format(round(disambiguation_time_end - disambiguation_time_start, 3)))
    print("    ------------------------------------------------------------")

def print_status():
    per_minute = round((TOTAL_PROCESSED_SENTENCES / (time.time() - start_time)) * 60)
    remaining = round((TOTAL_SENTENCES - TOTAL_PROCESSED_SENTENCES) / per_minute)
    print("{processed} / {total} sentences processed at a current rate of {per} sentences / minute".format(processed=TOTAL_PROCESSED_SENTENCES, total=TOTAL_SENTENCES, per=per_minute))
    print("Estimated {remaining} minutes remaining".format(remaining=remaining))

if __name__ == "__main__":
    # save script start time for basic performance measurement
    import time
    start_time = time.time()

    # connections to blazegraph.
    corpus = SPARQLWrapper("http://192.168.1.103:9999/blazegraph/sparql")
    #wordnet = SPARQLWrapper("http://192.168.1.103:9999/blazegraph/sparql")
    wordnet = corpus

    query = sparql.q_c_all_sentences(SENTENCE_LIMIT)
    result = sparql.run_query(corpus, query)
    sentences = [ value["uri"]["value"] for value in result["results"]["bindings"] ]

    TOTAL_SENTENCES = len(sentences)

    TOTAL_PROCESSED_SENTENCES = 0
    TOTAL_DISAMBIGUATED_WORDS = 0

    for s in sentences:
        ### Aufbereitung des Satzes
        target_words = []

        """Here we begin with preparation work for the current sentence.
        We query the DB for all words in the sentence, loop through every word obtained and check if it should be disambiguated.
        If not, we simply continue along the sentence.
        If it should be disambiguated, we collect all potential synsets by querying the DB using the lemmatized word and its provided part-of-speech tag.
        For every synset obtained in this way, we also query the DB for the synsets gloss.
        For every synset obtained we also query related synsets. Which relations to look for are defined in the RELATIONS dictionary above.
        We combine all glosses of the synsets obtained this way and also save them.

        This gives us a a fully prepared 'target_words' list which contains all the information we need to further disambiguate every word in the target sentence without hitting the DB again.
        The 'target_words' list will contain triples: (word_lemma, word_pos, synset_dict)
        'word_lemma' being the target words lemmatized form, 'word_pos' the target words part-of-speech tag and 'synset_dict' a dictionary containing glosses of all target and related synsets.
        """
        preparation_time_start = time.time()

        query = sparql.q_c_words_in_sentence(s)
        result = sparql.run_query(corpus, query)
        words = [ value for value in result["results"]["bindings"] ]

        for word in words:
            word_id = word["id"]["value"]
            word_lemma = word["lemma"]["value"]
            word_pos = word["upos"]["value"]

            if word_pos not in WORD_TYPES:
                continue

            synset_dict = {}

            query = sparql.q_w_get_synsets_for_word(word_lemma, word_pos)
            result = sparql.run_query(wordnet, query)
            synsets = [ value["synset"]["value"] for value in result["results"]["bindings"] ]
            for synset in synsets:
                relation_dict = {}

                query = sparql.q_w_get_gloss_for_synset(synset)
                result = sparql.run_query(wordnet, query)
                gloss = result["results"]["bindings"][0]["gloss"]["value"]
                relation_dict["gloss"] = gloss

                for REL in RELATIONS[word_pos] :
                    if REL != "gloss":
                        combined_gloss = []

                        query = sparql.q_w_get_related_synsets(synset, REL)
                        result = sparql.run_query(wordnet, query)
                        related_synsets = [ value["related"]["value"] for value in result["results"]["bindings"] ]

                        for rel_synset in related_synsets:
                            query = sparql.q_w_get_gloss_for_synset(rel_synset)
                            result = sparql.run_query(wordnet, query)
                            gloss = result["results"]["bindings"][0]["gloss"]["value"]
                            combined_gloss.append(gloss)
                    
                        relation_dict[REL] = " ".join(combined_gloss)
                synset_dict[synset] = relation_dict
            
            target_words.append((word_id, word_lemma, word_pos, synset_dict))

        preparation_time_end = time.time()

        print("Current sentence: {}".format(" ".join([ word["word"]["value"] for word in words ])))
        print("Preparation time: {} seconds".format(round(preparation_time_end - preparation_time_start)))
        print("Will disambiguate {} words".format(len(target_words)))

        """This is the main disambiguation phase. All words to be disambiguated are now collected within the 'target_words' list.
        We go through this list and look at each word in turn. The active word becomes our target word.
        We calculate a window of context based on the position of the word within the list.
        Then we calculate the SenseScore for each possible synset.
        """
        for i in range(0, len(target_words)):
            disambiguation_time_start = time.time()

            id, lemma, pos, synset_dict = target_words[i]

            best_sensescore = 0
            best_glosses = []

            if len(synset_dict) == 1:
                [(k, v)] = synset_dict.items() #um das einzige synset direkt zu finden
                best_glosses = [(k, v["gloss"])]
                disambiguation_time_end = time.time()
                write_results(best_glosses)
                print_results(single=True)
                continue
            if len(target_words) >= 3:
                if i == 0:
                    woc = [i+1, i+2]
                elif i == (len(target_words) - 1):
                    woc = [len(target_words) - 2, len(target_words) - 3]
                else:
                    woc = [i - 1, i + 1]
            else:
                print("Less than three target words. Continuing.")
                continue #TODO: Catch cases where sentence only has one or two words to be disambiguated!

            for synset in synset_dict:
                sensescore = 0

                for j in woc:
                    woc_id, woc_lemma, woc_pos, woc_synset_dict = target_words[j]

                    for woc_synset in woc_synset_dict:
                        for target_relation in RELATIONS[pos]:
                            for woc_relation in RELATIONS[woc_pos]:
                                if (len(synset_dict[synset][target_relation]) != 0) and (len(woc_synset_dict[woc_synset][woc_relation]) != 0):
                                    sensescore += overlap.overlap_score(synset_dict[synset][target_relation].split(" "), woc_synset_dict[woc_synset][woc_relation].split(" "))
                if sensescore > best_sensescore:
                    best_sensescore = sensescore
                    best_glosses = [(synset, synset_dict[synset]["gloss"])]
                elif sensescore == best_sensescore:
                    best_glosses.append((synset, synset_dict[synset]["gloss"]))

            disambiguation_time_end = time.time()
            TOTAL_DISAMBIGUATED_WORDS += 1

            write_results(best_glosses)

            print_results()

        print("**********************************************************************")
        TOTAL_PROCESSED_SENTENCES += 1
        print_status()
        print("**********************************************************************")
    print("")
    print("Total time for {} sentences: {} seconds".format(SENTENCE_LIMIT, round(time.time() - start_time)))
    print("Total disambiguated words: {}".format(TOTAL_DISAMBIGUATED_WORDS))
    print("Average time per word: {} seconds".format( round((round(time.time() - start_time)) / TOTAL_DISAMBIGUATED_WORDS, 1)))
